<?php
require_once realpath("vendor/autoload.php");

use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Client\RivrHttpClient;

/**
 * The mockmodel will respond to specific values in the request
 *
 * Use this template to interact with it and get familiar with the builder/flow.
 */

$client = new RivrHttpClient();
$request = RivrRequest::createBuilder()
    ->adSpotId("spot")
    ->appOrSite("")
    ->auctionType(0)
    ->browser("")
    ->browserVersion("")
    ->clientFloorPrice(0.1)
    ->clientFloorPriceFlexible(true)
    ->category("IAB-x")
    ->connectionType(2)
    ->context("")
    ->countryCode("XYZ")
    ->creativeSize("300x250")
    ->creativeType("banner")
    ->deviceType(4)
    ->deviceLanguage("")
    ->deviceManufacturer("")
    ->deviceModel("")
    ->libCallId(2)
    ->operatingSystem("")
    ->userId("")
    ->additionalProperty("random-key", "foobar")
    ->additionalProperty("dataCentre", "us-east-1")
    ->build();


$response = $client->optimize($request);

if ($response->requiresClientAction()) {
    print_r($response->getBidderIds());
    echo "do sth with dsps\n";
} else {
    echo "continue with your regular flow\n";
}

