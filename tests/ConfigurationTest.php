<?php

namespace Simplaex\Yieldoptimizer\Tests;

use PHPUnit\Framework\TestCase;
use Simplaex\Yieldoptimizer\Config\Configuration;

class ConfigurationTest extends TestCase
{

    public function testSettingServerEndpointHasNoImpact()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals("localhost:7487/v1/optimize", $config->url);
        $_SERVER["RIVR_SERVER_ENDPOINT"] = "http://raas.com";
        $config = Configuration::getConfiguration();
        $this->assertEquals("localhost:7487/v1/optimize", $config->url);
    }

    public function testSettingServerAuthHasNoImpact()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals(false, $config->auth);
        $_SERVER["RIVR_REMOTE_AUTH_TOKEN"] = "user:pass";
        $config = Configuration::getConfiguration();
        $this->assertEquals(false, $config->auth);
    }

    public function testSettingServerTimeoutHasNoImpact()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals(5, $config->timeout);
        $_SERVER["RIVR_RESPONSE_TIMEOUT"] = 100;
        $config = Configuration::getConfiguration();
        $this->assertEquals(5, $config->timeout);
    }


    public function testDefaultEndpointFromEnv()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals("localhost:7487/v1/optimize", $config->url);
        $_ENV["RIVR_SERVER_ENDPOINT"] = "http://raas.com";
        $config = Configuration::getConfiguration();
        $this->assertEquals("http://raas.com", $config->url);
    }

    public function testDefaultAuthFromEnv()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals(false, $config->auth);
        $_ENV["RIVR_REMOTE_AUTH_TOKEN"] = "user:pass";
        $config = Configuration::getConfiguration();
        $this->assertEquals("user:pass", $config->auth);
    }

    public function testDefaultTimeoutFromEnv()
    {
        $config = Configuration::getConfiguration();
        $this->assertEquals(5, $config->timeout);
        $_ENV["RIVR_RESPONSE_TIMEOUT"] = 100;
        $config = Configuration::getConfiguration();
        $this->assertEquals(100, $config->timeout);
    }
}