<?php

namespace Simplaex\Yieldoptimizer\Tests;

use PHPUnit\Framework\TestCase;
use Simplaex\Yieldoptimizer\Config\Configuration;
use Simplaex\Yieldoptimizer\Connector\HttpConnector;
use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Client\RivrHttpClient;
use Simplaex\Yieldoptimizer\Tests\Util\ConnectorUtil;

class RivrErrorResponseTest extends TestCase
{
    public function testGenerateResponse()
    {
        $client = new RivrHttpClient;
        $_ENV["RIVR_SERVER_ENDPOINT"] = 'http://idontexistaaaa.com';
        $response = $client->generateResponse("", 7);
        $this->assertEquals("REQUEST_TIMEOUT", $response->optimization);
        $response = $client->generateResponse("", 28);
        $this->assertEquals("REQUEST_TIMEOUT", $response->optimization);
    }

    public function testHttpConnector()
    {
        $config = new Configuration();
        $config->url = "https://www.rivrai.com";
        $config->timeout = 1;
        $client = new HttpConnector($config);
        list($resp, $curlerr, $httperr) = $client->request("foobar");
        $this->assertEquals(28, $curlerr);
        // here we should also assert some value for ext, ideally we want it to debug those parsing-errors
    }

    public function testResponseWithMissingValues()
    {
        $client = new RivrHttpClient(ConnectorUtil::generateConnector("", 0));
        $response = $client->optimize($this->getRequest());
        $this->assertEquals("INTERNAL_PARSING_ERROR", $response->optimization);
        // here we should also assert some value for ext, ideally we want it to debug those parsing-errors
    }

    public function testTimeoutResponse()
    {
        $expectedExt = "{\"error\":\"REQUEST_TIMEOUT\",\"curlerr\":28}";
        $client = new RivrHttpClient(ConnectorUtil::generateConnector("", 28));
        $response = $client->optimize($this->getRequest());
        $ext = base64_decode($response->ext);
        $this->assertEquals("REQUEST_TIMEOUT", $response->optimization);
        $this->assertEquals($expectedExt, $ext);
    }

    public function testMissingBidderResponse()
    {
        $client = new RivrHttpClient(ConnectorUtil::generateConnector(ConnectorUtil::$MISSING_BIDDER_RESPONSE, 0));
        $response = $client->optimize($this->getRequest());
        $this->assertEquals(array(), $response->dsps);
    }

    public function testEmptyBidderResponse()
    {
        $client = new RivrHttpClient(ConnectorUtil::generateConnector(ConnectorUtil::$EMPTY_BIDDER_RESPONSE, 0));
        $response = $client->optimize($this->getRequest());
        $this->assertEquals(0, count($response->dsps));
    }

    function getRequest()
    {
        return RivrRequest::createBuilder()
            ->appOrSite("07th-expansion.fandom.com")
            ->countryCode("US")
            ->deviceType(1)
            ->browser("Chrome")
            ->creativeType("banner")
            ->additionalProperty("SspInternalId", "16")
            ->additionalProperty("dataCentre", "us-east-1")
            ->build();
    }
}