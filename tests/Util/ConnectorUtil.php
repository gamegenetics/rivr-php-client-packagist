<?php

namespace Simplaex\Yieldoptimizer\Tests\Util;

use Simplaex\Yieldoptimizer\Connector\Connector;

class ConnectorUtil
{
    public static $VALID_RESPONSE = <<<EOD
    {
        "optimization":"RECOMMENDED_DSPS_ONLY",
        "dsps":[
           {
              "bidderId":"43"
           },
           {
              "bidderId":"56"
           },
           {
              "bidderId":"66"
           },
           {
              "bidderId":"68"
           },
           {
              "bidderId":"15"
           },
           {
              "bidderId":"33"
           },
           {
              "bidderId":"34"
           },
           {
              "bidderId":"53"
           },
           {
              "bidderId":"63"
           },
           {
              "bidderId":"73"
           },
           {
              "bidderId":"14"
           },
           {
              "bidderId":"60"
           },
           {
              "bidderId":"72"
           },
           {
              "bidderId":"22"
           },
           {
              "bidderId":"48"
           },
           {
              "bidderId":"59"
           },
           {
              "bidderId":"57"
           },
           {
              "bidderId":"62"
           },
           {
              "bidderId":"69"
           },
           {
              "bidderId":"50"
           }
        ],
        "ext":"eyJvcHRpbWl6ZSI6dHJ1ZSwiY29uZmlndXJlZF90b19vcHRpbWl6ZSI6dHJ1ZSwidmVyc2lvbiI6eyJhbGlhcyI6Im5ld19xcHMifSwidHJpbW1lZF9yZXF1ZXN0Ijp7Ik9wZXJhdGluZ1N5c3RlbSI6Ik90aGVyIiwiQnJvd3NlciI6IkNocm9tZSIsIkFkZGl0aW9uYWxQcm9wZXJ0aWVzIjp7ImRhdGFjZW50cmUiOiJ1cy1lYXN0LTEiLCJuX3Jlc3BvbnNlcyI6IjAiLCJyZXZlbnVlIjoiMC4wIiwibl9hdWN0aW9ucyI6IjEiLCJpbnZlbnRvcnl0eXBlIjoib3duIiwibl9yZXF1ZXN0cyI6IjEiLCJzc3BpbnRlcm5hbGlkIjoiMTIiLCJiaWRkZXJpZCI6IjUwIiwibl93aW5zIjoiMCIsImJpZF9wcmljZV9zdW0iOiIwLjAifX19"
    }
    EOD;

    public static $MISSING_BIDDER_RESPONSE = <<<EOD
    {
        "optimization":"RECOMMENDED_DSPS_ONLY",
        "ext":"eyJvcHRpbWl6ZSI6dHJ1ZSwiY29uZmlndXJlZF90b19vcHRpbWl6ZSI6dHJ1ZSwidmVyc2lvbiI6eyJhbGlhcyI6Im5ld19xcHMifSwidHJpbW1lZF9yZXF1ZXN0Ijp7Ik9wZXJhdGluZ1N5c3RlbSI6Ik90aGVyIiwiQnJvd3NlciI6IkNocm9tZSIsIkFkZGl0aW9uYWxQcm9wZXJ0aWVzIjp7ImRhdGFjZW50cmUiOiJ1cy1lYXN0LTEiLCJuX3Jlc3BvbnNlcyI6IjAiLCJyZXZlbnVlIjoiMC4wIiwibl9hdWN0aW9ucyI6IjEiLCJpbnZlbnRvcnl0eXBlIjoib3duIiwibl9yZXF1ZXN0cyI6IjEiLCJzc3BpbnRlcm5hbGlkIjoiMTIiLCJiaWRkZXJpZCI6IjUwIiwibl93aW5zIjoiMCIsImJpZF9wcmljZV9zdW0iOiIwLjAifX19"
    }
    EOD;

    public static $EMPTY_BIDDER_RESPONSE = <<<EOD
    {
        "optimization":"RECOMMENDED_DSPS_ONLY",
        "dsps":[],
        "ext":"eyJvcHRpbWl6ZSI6dHJ1ZSwiY29uZmlndXJlZF90b19vcHRpbWl6ZSI6dHJ1ZSwidmVyc2lvbiI6eyJhbGlhcyI6Im5ld19xcHMifSwidHJpbW1lZF9yZXF1ZXN0Ijp7Ik9wZXJhdGluZ1N5c3RlbSI6Ik90aGVyIiwiQnJvd3NlciI6IkNocm9tZSIsIkFkZGl0aW9uYWxQcm9wZXJ0aWVzIjp7ImRhdGFjZW50cmUiOiJ1cy1lYXN0LTEiLCJuX3Jlc3BvbnNlcyI6IjAiLCJyZXZlbnVlIjoiMC4wIiwibl9hdWN0aW9ucyI6IjEiLCJpbnZlbnRvcnl0eXBlIjoib3duIiwibl9yZXF1ZXN0cyI6IjEiLCJzc3BpbnRlcm5hbGlkIjoiMTIiLCJiaWRkZXJpZCI6IjUwIiwibl93aW5zIjoiMCIsImJpZF9wcmljZV9zdW0iOiIwLjAifX19"
    }
    EOD;


    public static function generateConnector($json, $error, $httpstatus = 200): Connector
    {
        return new class($json, $error, $httpstatus) implements Connector {
            public $json;
            public $error;
            private $httpstatus;

            public function __construct($json, $error, $httpstatus)
            {
                $this->json = $json;
                $this->error = $error;
                $this->httpstatus = $httpstatus;
            }

            function request(?string $data = null): array
            {
                return [$this->json, $this->error, $this->httpstatus];
            }

        };
    }
}