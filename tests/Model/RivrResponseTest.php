<?php

namespace Simplaex\Yieldoptimizer\Tests;

use Simplaex\Yieldoptimizer\Model\RivrResponse;
use PHPUnit\Framework\TestCase;
use Simplaex\Yieldoptimizer\Client\RivrHttpClient;
use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Tests\Util\ConnectorUtil;

class RivrResponseTest extends TestCase
{
    public function testWrapperForErrorCases()
    {
        $response = RivrResponse::noresponseError("Foobar", 27);
        $this->assertEquals("Foobar", $response->optimization);
        $this->assertEquals(array(), $response->dsps);
        $extdecoded = base64_decode($response->ext);
        $this->assertStringContainsString("\"error\":\"Foobar\"", $extdecoded);
    }

    public function testValidResponse()
    {
        $request = RivrRequest::createBuilder()->build();
        $client = new RivrHttpClient(ConnectorUtil::generateConnector(ConnectorUtil::$VALID_RESPONSE, 0));
        $response = $client->optimize($request);
        $this->assertEquals("RECOMMENDED_DSPS_ONLY", $response->optimization);
        $this->assertEquals(20, count($response->dsps));
        $this->assertEquals(true, in_array("43", array_column($response->dsps, "bidderId"), true));
        $this->assertEquals(false, in_array("300", array_column($response->dsps, "bidderId"), true));
        $this->assertContains("43", $response->getBidderIds());
        $this->assertContains("15", $response->getBidderIds());
        $this->assertNotContains("300", $response->getBidderIds());

        $this->assertEquals("eyJvcHRpbWl6ZSI6dHJ1ZSwiY29uZmlndXJlZF90b19vcHRpbWl6ZSI6dHJ1ZSwidmVyc2lvbiI6eyJhbGlhcyI6Im5ld19xcHMifSwidHJpbW1lZF9yZXF1ZXN0Ijp7Ik9wZXJhdGluZ1N5c3RlbSI6Ik90aGVyIiwiQnJvd3NlciI6IkNocm9tZSIsIkFkZGl0aW9uYWxQcm9wZXJ0aWVzIjp7ImRhdGFjZW50cmUiOiJ1cy1lYXN0LTEiLCJuX3Jlc3BvbnNlcyI6IjAiLCJyZXZlbnVlIjoiMC4wIiwibl9hdWN0aW9ucyI6IjEiLCJpbnZlbnRvcnl0eXBlIjoib3duIiwibl9yZXF1ZXN0cyI6IjEiLCJzc3BpbnRlcm5hbGlkIjoiMTIiLCJiaWRkZXJpZCI6IjUwIiwibl93aW5zIjoiMCIsImJpZF9wcmljZV9zdW0iOiIwLjAifX19", $response->ext);
    }

    public function testClientActionBasedOffOptimization()
    {
        $mapping = [
            "RECOMMENDED" => true,
            "RECOMMENDED_DSPS_ONLY" => true,
            "RECOMMENDED_PRICE_ONLY" => true,
            "NOT_AVAILABLE" => false,
            "NOT_RECOMMENDED" => true,
            "NOT_OPTIMIZED" => false,
            "MODEL_EXPIRED" => false,
            "MODEL_NOT_LOADED" => false,
            "INTERNAL_CONVERSION_ERROR" => false,
            "INTERNAL_OPTIMIZATION_ERROR" => false,
            "INTERNAL_PARSING_ERROR" => false,
            "REQUEST_TIMEOUT" => false,
            "REQUEST_REJECTED" => false,
            "" => false,
            "garbage" => false,
            0 => false,
            3283927 => false
        ];

        foreach ($mapping as $optimization => $actionable) {
            $response = RivrResponse::noresponseError($optimization, 0);
            $this->assertEquals($actionable, $response->requiresClientAction());
        }
    }
}



