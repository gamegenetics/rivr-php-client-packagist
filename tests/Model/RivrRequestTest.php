<?php

namespace Simplaex\Yieldoptimizer\Tests;

use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Model\RivrRequestBuilder;
use PHPUnit\Framework\TestCase;

class RivrRequestTest extends TestCase
{
    public function testRequest()
    {
        $model = new RivrRequestBuilder;
        $appOrSite = $model->appOrSite("Test-APP")->build()->AppOrSite;
        $this->assertEquals("Test-APP", $appOrSite);
    }

    public function testJsonParsing()
    {
        $model = new RivrRequestBuilder;
        $jsonblob = $model->appOrSite("Test-APP")->build()->toJson();
        $this->assertStringContainsString("\"AppOrSite\":\"Test-APP\"", $jsonblob);
        $this->assertStringContainsString("\"AdditionalProperties\":{}", $jsonblob);         // do we expect no key at all
    }

    public function testSomething()
    {
        $model = new RivrRequestBuilder;
        $jsonblob = $model->build()->toJson();
        //$this->assertStringContainsString("\"AppOrSite\":\"\"", $jsonblob);         // do we expect empty string
        $this->assertStringNotContainsString("\"AppOrSite\"", $jsonblob);         // do we expect no key at all
    }

    public function testNonPresent()
    {
        $model = new RivrRequestBuilder;
        $jsonblob = $model->libCallId(2)->build()->toJson();
        $this->assertStringContainsString("\"LibCallId\":2", $jsonblob);         // do we expect empty string
    }

    public function testRequestFields()
    {
        $model = new RivrRequestBuilder;
        $jsonblob = $model->libCallId(2)
            ->countryCode("GER")
            ->deviceType(1)
            ->deviceModel("test-device")
            ->deviceManufacturer("test-deviceManufacturer")
            ->adSpotId("test-adSpotId")
            ->deviceLanguage("DEU")
            ->browserVersion("test-browserVersion")
            ->operatingSystemVersion("test-os-version")
            ->additionalProperty("SOMETHINGBIG", "value")
            ->build()->toJson();

        $this->assertStringContainsString("\"CountryCode\":\"GER\"", $jsonblob);
        $this->assertStringContainsString("\"DeviceType\":1", $jsonblob);
        $this->assertStringContainsString("\"DeviceModel\":\"test-device\"", $jsonblob);
        $this->assertStringContainsString("\"DeviceManufacturer\":\"test-deviceManufacturer\"", $jsonblob);
        $this->assertStringContainsString("\"AdSpotId\":\"test-adSpotId\"", $jsonblob);
        $this->assertStringContainsString("\"DeviceLanguage\":\"DEU\"", $jsonblob);
        $this->assertStringContainsString("\"OperatingSystemVersion\":\"test-os-version\"", $jsonblob);
        $this->assertStringContainsString("\"BrowserVersion\":\"test-browserVersion\"", $jsonblob);
        $this->assertStringContainsString("\"AdditionalProperties\":{\"somethingbig\":\"value\"}", $jsonblob);
    }

    public function testDeviceType()
    {
        $builder = new RivrRequestBuilder;
        //Known
        $jsonblob = $builder->deviceType(1)->build()->toJson();
        $this->assertStringContainsString("\"DeviceType\":1", $jsonblob);
        //Unknown
        $jsonblob = $builder->deviceType(100)->build()->toJson();
        $this->assertStringContainsString("\"DeviceType\":100", $jsonblob);
    }

    public function testConnectionType()
    {
        $builder = new RivrRequestBuilder;
        //Known
        $jsonblob = $builder->connectionType(1)->build()->toJson();
        $this->assertStringContainsString("\"ConnectionType\":1", $jsonblob);
        //Unknown
        $jsonblob = $builder->connectionType(100)->build()->toJson();
        $this->assertStringContainsString("\"ConnectionType\":100", $jsonblob);
    }

    public function testAuctionType()
    {
        $builder = new RivrRequestBuilder;
        //Known
        $jsonblob = $builder->auctionType(1)->build()->toJson();
        $this->assertStringContainsString("\"AuctionType\":1", $jsonblob);
        //Unknown
        $jsonblob = $builder->auctionType(100)->build()->toJson();
        $this->assertStringContainsString("\"AuctionType\":100", $jsonblob);
    }

    public function testContextType()
    {
        $builder = new RivrRequestBuilder;
        $jsonblob = $builder->context(1)->build()->toJson();
        $this->assertStringContainsString("\"Context\":1", $jsonblob);
        $jsonblob = $builder->context("foobar")->build()->toJson();
        $this->assertStringContainsString("\"Context\":\"foobar\"", $jsonblob);
    }

    public function testCreativeTypeGuards()
    {
        $sub = new RivrRequestBuilder;
        $request = $sub->creativeType("BANNER")->build()->toJson();
        $this->assertStringContainsString("\"CreativeType\":[\"BANNER\"]", $request);
        $request = $sub->creativeType("fooBAR_123")->build()->toJson();
        $this->assertStringContainsString("\"CreativeType\":[\"BANNER\",\"fooBAR_123\"]", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->creativeTypes([])->build()->toJson();
        $this->assertStringNotContainsString("\"CreativeType\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->creativeTypes(array("BANNER", "NATIVE"))->build()->toJson();
        $this->assertStringContainsString("\"CreativeType\":[\"BANNER\",\"NATIVE\"]", $request);
    }

    public function testCreativeSizeGuards()
    {
        $sub = new RivrRequestBuilder;
        $request = $sub->creativeSize("1x1")->build()->toJson();
        $this->assertStringContainsString("\"CreativeSize\":[\"1x1\"]", $request);
        $request = $sub->creativeSize("0x0")->build()->toJson();
        $this->assertStringContainsString("\"CreativeSize\":[\"1x1\",\"0x0\"]", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->creativeSizes([])->build()->toJson();
        $this->assertStringNotContainsString("\"CreativeSize\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->creativeSizes(array("2x2", "100x200"))->build()->toJson();
        $this->assertStringContainsString("\"CreativeSize\":[\"2x2\",\"100x200\"]", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->creativeSize("AxB")->build()->toJson();
        $this->assertStringNotContainsString("\"CreativeSize\"", $request);
    }

    public function testOperatingSystemMappings()
    {
        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("Foobar")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"Other\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("xxChromebookfibarxx")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"Chrome\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("iosIOSiOSiPhoneOS")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"iOS\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("StarTrekAndroidSystem")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"Android\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("12badLiNuX21321")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"Linux\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->operatingSystem("somewinfoobar")->build()->toJson();
        $this->assertStringContainsString("\"OperatingSystem\":\"Windows\"", $request);
    }

    public function testDifferentTypesForSomeFields()
    {
        $sub = new RivrRequestBuilder;
        $request = $sub->deviceType("foobar")->build()->toJson();
        $this->assertStringContainsString("\"DeviceType\":\"foobar\"", $request);

        $sub = new RivrRequestBuilder;
        $request = $sub->deviceType(123)->build()->toJson();
        $this->assertStringContainsString("\"DeviceType\":123", $request);
    }

    public function testDocumentationRequest()
    {
        $request = RivrRequest::createBuilder()
            ->adSpotId("416776")
            ->appOrSite("1002275138")
            ->auctionType(2)
            ->browser("Safari")
            ->browserVersion("12.345.6")
            ->clientFloorPrice(12.34)
            ->clientFloorPriceFlexible(true)
            ->category("IAB-2")
            ->connectionType(2)
            ->context(4)
            ->countryCode("CAN")
            ->creativeSize("300x250")
            ->creativeType("banner")
            ->deviceType(4)
            ->deviceLanguage("US")
            ->deviceManufacturer("company")
            ->deviceModel("brick")
            ->libCallId(2)
            ->locationType(3)
            ->operatingSystem("iOS")
            ->userId("uuid-123-xyz")
            ->additionalProperty("random-key", "foobar")
            ->additionalProperty("dataCentre", "us-east-1")
            ->build();

        $this->assertNotEmpty($request->toJson());


        $singular = RivrRequest::createBuilder()
            ->category("single IAB")
            ->category("IAB-secondary")
            ->creativeSize("200x300")
            ->creativeSize("50x250")
            ->creativeType("video")
            ->creativeType("banner")
            ->build();

        $plural = RivrRequest::createBuilder()
            ->categories(array("single IAB", "IAB-secondary"))
            ->creativeSizes(array("200x300", "50x250"))
            ->creativeTypes(array("video", "banner"))
            ->build();

        $this->assertEquals($singular->toJson(), $plural->toJson());
    }
}