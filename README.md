## RIVR PHP CLIENT

A simple wrapper to easily construct RivrRequests, optimize them and get a RivrResponse.

Simply run the rivr-server locally or test against rivr-as-a-service.

### Basic usage

```php
<?php

use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Client\RivrHttpClient;

$client = new RivrHttpClient();             // You can reuse the client for as many requests as you like
$request = RivrRequest::createBuilder()
    ->appOrSite("App or Site")
    ->userId("userid-xyz")
    ->browser("choose")
    ->additionalProperty("random-key", "foobar")
    ->additionalProperty("dataCentre", "us-east-1")
    ->build();


$response = $client->optimize($request);

if ($response->requiresClientAction()) {
    print_r($response->getBidderIds());
    echo "do sth with dsps\n";
} else {
    echo "continue with your regular flow\n";
}

```

### Debugging + Logging
By default rivr will not emit any logs using the client. In order to debug potential issues, you simply provide your prefered logger that implements the `Psr\Log\LoggerInterface`, define your desired log-level and the client will emit logs.

```php
<?php

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;
use Simplaex\Yieldoptimizer\Logging\LoggingMiddleware;
use Simplaex\Yieldoptimizer\Client\RivrHttpClient;

$logger = new Logger('Rivr');
$logger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::INFO));

LoggingMiddleware::initLogger($logger);
$client = new RivrHttpClient();

// When running will emit something like:
[2021-05-26T15:56:30.298900+02:00] Rivr.INFO: Timeout set to 5 ms. [] []
[2021-05-26T15:56:30.300092+02:00] Rivr.INFO: Autoconfig done. Sending all requests to localhost:7487/v1/optimize [] []

```


### Configuration Options
A number of environment variables are available to alter the behavior of the validator. 

| Flag | Description |
|------|-------------|
| `RIVR_SERVER_ENDPOINT` | endpoint to send request to, defaults to localhost:7487 |
| `RIVR_REMOTE_AUTH_TOKEN` | when using a different endpoint, credentials might be necessary |
| `RIVR_RESPONSE_TIMEOUT` | amount of time in ms to wait for a response, defaults to 5ms but change to >500ms when testing with rivr-as-a-service |

For more information about the whole ecosystem refer to the complete integration guide.