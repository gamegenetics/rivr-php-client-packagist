<?php

namespace Simplaex\Yieldoptimizer\Config;

use Simplaex\Yieldoptimizer\Logging\LoggingMiddleware;

class Configuration
{
    public $timeout;
    public $url;
    public $auth;

    public static function getConfiguration(): Configuration
    {
        /**
         * Autoconfiguration - determine required parameters from environment or use defaults
         *
         * RIVR_SERVER_ENDPOINT
         *  address to sent requests to, includes the portnumber and path, e.g. https://integration-demo.rivrai.com/v1/optimize
         *  default: localhost:7487/v1/optimize
         */
        if (isset($_ENV["RIVR_SERVER_ENDPOINT"])) {
            $rivr_server_endpoint = $_ENV["RIVR_SERVER_ENDPOINT"];
            LoggingMiddleware::info("Overriding environment variable RIVR_SERVER_ENDPOINT with $rivr_server_endpoint");
        } else {
            $rivr_server_endpoint = "localhost:7487/v1/optimize";
            LoggingMiddleware::debug("Environment variable RIVR_SERVER_ENDPOINT not set. Defaulting to $rivr_server_endpoint");
        }

        /**
         * RIVR_REMOTE_AUTH
         *  when using rivr-as-a-service, which basic auth token to use, e.g. rivruser:password
         *  default: <unset>
         */
        if (isset($_ENV["RIVR_REMOTE_AUTH_TOKEN"])) {
            $rivr_remote_auth_token = $_ENV["RIVR_REMOTE_AUTH_TOKEN"];
            LoggingMiddleware::info("Using Environment variable RIVR_REMOTE_AUTH_TOKEN for authentication.");
        } else {
            $rivr_remote_auth_token = false;
            LoggingMiddleware::debug("Environment variable RIVR_REMOTE_AUTH_TOKEN is not set. $rivr_server_endpoint will be accessed without any Credentials.");
        }

        /**
         * RIVR_RESPONSE_TIMEOUT_TOKEN
         *  timeout in ms for how long to wait for a response from the server
         *  default: 5 (ms)
         */
        $rivr_timeout_ms = (int) ($_ENV["RIVR_RESPONSE_TIMEOUT"] ?? 5);
        LoggingMiddleware::info("Timeout set to $rivr_timeout_ms ms.");
        LoggingMiddleware::info("Autoconfig done. Sending all requests to $rivr_server_endpoint");

        $config = new Configuration();
        $config->timeout = $rivr_timeout_ms;
        $config->auth = $rivr_remote_auth_token;
        $config->url = $rivr_server_endpoint;

        return $config;
    }
}
