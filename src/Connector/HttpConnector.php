<?php

namespace Simplaex\Yieldoptimizer\Connector;

use Simplaex\Yieldoptimizer\Config\Configuration;
use Simplaex\Yieldoptimizer\Logging\LoggingMiddleware;

class HttpConnector implements Connector
{
    private $curl;

    public function __construct(Configuration $config)
    {
        $this->curl = curl_init($config->url);

        $options = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT_MS     => $config->timeout,
            CURLOPT_HTTPHEADER     => ["Content-Type: application/json"],
        ];

        if ($config->auth) {
            $options[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
            $options[CURLOPT_USERPWD]  = $config->auth;
        }

        curl_setopt_array($this->curl, $options);
    }

    public function request(?string $data = null): array
    {
        if ($data !== null) {
            curl_setopt_array($this->curl, [
                CURLOPT_POST       => 1,
                CURLOPT_POSTFIELDS => $data,
            ]);
        }

        $result = curl_exec($this->curl);
        $curl_errno = curl_errno($this->curl);

        if ($curl_errno === 0) {
            $http_status = curl_getinfo($this->curl, CURLINFO_RESPONSE_CODE);
            if ($http_status !== 200) {
                LoggingMiddleware::info("Unexpected Httpstatus: $http_status");
                LoggingMiddleware::debug("curl_getinfo", curl_getinfo($this->curl));
            }
        } else {
            $http_status = 500;
            LoggingMiddleware::info(sprintf("Unexpected Curlresult (%s): %s", $curl_errno, curl_error($this->curl)));
            LoggingMiddleware::debug("curl_getinfo", curl_getinfo($this->curl));
        }
        return [$result, $curl_errno, $http_status];
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }
}
