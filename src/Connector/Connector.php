<?php

namespace Simplaex\Yieldoptimizer\Connector;

interface Connector
{
    public function request(?string $data = null): array;
}