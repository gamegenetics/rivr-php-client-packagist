<?php

namespace Simplaex\Yieldoptimizer\Client;

use Simplaex\Yieldoptimizer\Connector\HttpConnector;
use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Model\RivrResponse;
use Simplaex\Yieldoptimizer\Config\Configuration;
use Simplaex\Yieldoptimizer\Connector\Connector;

class RivrHttpClient implements Client
{

    public $connector;

    public function __construct(Connector $connector = null)
    {
        if ($connector === null) {
            $this->connector = new HttpConnector(Configuration::getConfiguration());
        } else {
            $this->connector = $connector;
        }
    }

    public function optimize(RivrRequest $request): RivrResponse
    {
        $jsin = $request->toJson();
        [$json, $curl_errno, $http_status] = $this->connector->request($jsin);
        return $this->generateResponse($json, $curl_errno, $http_status);
    }

    public function generateResponse(string $json, int $curl_errno = 0, int $http_status = 200): RivrResponse
    {
        if ($curl_errno !== 0) {
            return RivrResponse::noresponseError("REQUEST_TIMEOUT", $curl_errno);
        } elseif ($http_status !== 200) {
            return RivrResponse::noresponseError("INTERNAL_PARSING_ERROR", $http_status);
        }

        return new RivrResponse($json);
    }
}
