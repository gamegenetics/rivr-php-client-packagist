<?php

namespace Simplaex\Yieldoptimizer\Client;

use Simplaex\Yieldoptimizer\Model\RivrRequest;
use Simplaex\Yieldoptimizer\Model\RivrResponse;

interface Client
{
    public function optimize(RivrRequest $request): RivrResponse;
}
