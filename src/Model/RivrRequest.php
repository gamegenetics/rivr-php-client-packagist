<?php

namespace Simplaex\Yieldoptimizer\Model;

class RivrRequest
{

    public static function createBuilder(): RivrRequestBuilder
    {
        return new RivrRequestBuilder();
    }

    public function __construct(RivrRequestBuilder $b)
    {
        $this->AdSpotId                 = $b->getAdSpotId();
        $this->AppOrSite                = $b->getAppOrSite();
        $this->AuctionType              = $b->getAuctionType();
        $this->Categories               = $b->getCategories();
        $this->ClientFloorPriceFlexible = $b->getClientFloorPriceFlexible();
        $this->ClientFloorPrice         = $b->getClientFloorPrice();
        $this->Context                  = $b->getContext();
        $this->CountryCode              = $b->getCountryCode();
        $this->CreativeType             = $b->getCreativeTypes();
        $this->CreativeSize             = $b->getCreativeSizes();
        $this->Browser                  = $b->getBrowser();
        $this->BrowserVersion           = $b->getBrowserVersion();
        $this->ConnectionType           = $b->getConnectionType();
        $this->DeviceLanguage           = $b->getDeviceLanguage();
        $this->DeviceModel              = $b->getDeviceModel();
        $this->DeviceManufacturer       = $b->getDeviceManufacturer();
        $this->DeviceType               = $b->getDeviceType();
        $this->LibCallId                = $b->getLibCallId();
        $this->Location                 = $b->getLocation();
        $this->LocationType             = $b->getLocationType();
        $this->OperatingSystem          = $b->getOperatingSystem();
        $this->OperatingSystemVersion   = $b->getOperatingSystemVersion();
        $this->UserId                   = $b->getUserId();
        $this->AdditionalProperties     = $b->getAdditionalProperties();
    }

    public function toJson(): string
    {
        $nonulls = array_filter((array) $this);
        if (!isset($nonulls['AdditionalProperties'])) {
            $nonulls["AdditionalProperties"] = (object) [];
        }
        return json_encode($nonulls);
    }
}


