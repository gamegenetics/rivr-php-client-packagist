<?php

namespace Simplaex\Yieldoptimizer\Model;

class RivrResponse
{
    private const ACTIONABLES = [
        'RECOMMENDED'            => 1,
        'RECOMMENDED_DSPS_ONLY'  => 1,
        'RECOMMENDED_PRICE_ONLY' => 1,
        'NOT_RECOMMENDED'        => 1,
    ];

    public $ext;
    public $optimization;
    public $dsps = [];

    public function __construct($json = false)
    {
        $this->optimization = "INTERNAL_PARSING_ERROR";

        if ($json) {
            $this->set(json_decode($json, true));
        }
    }

    private function set($data): void
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function requiresClientAction(): bool
    {
        return isset(self::ACTIONABLES[$this->optimization]);
    }

    public function getBidderIds(): array
    {
        return array_column($this->dsps, 'bidderId');
    }

    public static function noresponseError($response, $curl_errno): RivrResponse
    {
        $res = new RivrResponse();
        $res->optimization = $response;
        $x = ['error' => $response, 'curlerr' => $curl_errno];
        $res->ext = base64_encode(json_encode($x));
        return $res;
    }
}
