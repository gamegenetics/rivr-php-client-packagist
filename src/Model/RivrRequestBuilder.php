<?php

namespace Simplaex\Yieldoptimizer\Model;

use Simplaex\Yieldoptimizer\Logging\LoggingMiddleware;

class RivrRequestBuilder
{
    private const VALID_DEVICE_TYPES     = [1 => 1, 2 => 1, 3 => 1, 4 => 1, 5 => 1, 6 => 1];
    private const VALID_CONNECTION_TYPES = [0 => 1, 1 => 1, 2 => 1, 3 => 1, 4 => 1, 5 => 1, 6 => 1];
    private const VALID_AUCTION_TYPES    = [0 => 1, 1 => 1, 2 => 1];

    private $AdSpotId;
    private $AppOrSite;
    private $AuctionType;
    private $Browser;
    private $BrowserVersion;
    private $Categories;
    private $ClientFloorPriceFlexible;
    private $ClientFloorPrice;
    private $Context;
    private $CountryCode;
    private $CreativeTypes;
    private $CreativeSizes;
    private $ConnectionType;
    private $DeviceLanguage;
    private $DeviceModel;
    private $DeviceManufacturer;
    private $DeviceType;
    private $LibCallId;
    private $Location;
    private $LocationType;
    private $OperatingSystem;
    private $OperatingSystemVersion;
    private $UserId;
    private $AdditionalProperties;

    public function __construct()
    {
        $this->Categories = [];
        $this->CreativeSizes = [];
        $this->CreativeTypes = [];
        $this->AdditionalProperties = [];
    }

    public function build(): RivrRequest
    {
        return new RivrRequest($this);
    }

    public function adSpotId(string $a): RivrRequestBuilder
    {
        $this->AdSpotId = $a;
        return $this;
    }

    public function appOrSite(string $a): RivrRequestBuilder
    {
        $this->AppOrSite = $a;
        return $this;
    }

    public function auctionType(int $a): RivrRequestBuilder
    {
        if (!isset(self::VALID_AUCTION_TYPES[$a])) {
            LoggingMiddleware::debug("Unknown auctionType");
        }
        $this->AuctionType = $a;
        return $this;
    }

    public function browser(string $a): RivrRequestBuilder
    {
        $this->Browser = $a;
        return $this;
    }

    public function browserVersion(string $a): RivrRequestBuilder
    {
        $this->BrowserVersion = $a;
        return $this;
    }

    public function categories(array $a): RivrRequestBuilder
    {
        foreach ($a as $item) {
            $this->category($item);
        }
        return $this;
    }

    public function category(string $a): RivrRequestBuilder
    {
        $this->Categories[] = $a;
        return $this;
    }

    public function clientFloorPriceFlexible(bool $a): RivrRequestBuilder
    {
        $this->ClientFloorPriceFlexible = $a;
        return $this;
    }

    public function clientFloorPrice(?float $a): RivrRequestBuilder
    {
        $this->ClientFloorPrice = $a;
        return $this;
    }

    # once php8.0 is used, provide typehint for int|string
    public function context($a): RivrRequestBuilder
    {
        $this->Context = $a;
        return $this;
    }

    public function countryCode(string $a): RivrRequestBuilder
    {
        $this->CountryCode = $a;
        return $this;
    }

    public function creativeTypes(array $a): RivrRequestBuilder
    {
        foreach ($a as $item) {
            $this->creativeType($item);
        }
        return $this;
    }

    public function creativeType(string $a): RivrRequestBuilder
    {
        array_push($this->CreativeTypes, $a);
        return $this;
    }

    public function creativeSizes(array $a): RivrRequestBuilder
    {
        foreach ($a as $item) {
            $this->creativeSize($item);
        }
        return $this;
    }

    public function creativeSize(string $a): RivrRequestBuilder
    {
        if (preg_match("/[0-9]+x[0-9]+/", $a)) {
            $this->CreativeSizes[] = $a;
        } else {
            LoggingMiddleware::warning("Creative size has to be in the form of \"WxH\", W and H being numbers. Provided $a");
        }
        return $this;
    }

    public function connectionType($a): RivrRequestBuilder
    {
        if (!isset(self::VALID_CONNECTION_TYPES[$a])) {
            LoggingMiddleware::debug("Unknown connectionType");
        }
        $this->ConnectionType = $a;
        return $this;
    }

    public function deviceLanguage(?string $a): RivrRequestBuilder
    {
        $this->DeviceLanguage = $a;
        return $this;
    }

    public function deviceModel(?string $a): RivrRequestBuilder
    {
        $this->DeviceModel = $a;
        return $this;
    }

    public function deviceManufacturer(?string $a): RivrRequestBuilder
    {
        $this->DeviceManufacturer = $a;
        return $this;
    }

    # once php8.0 is used, provide typehint for int|string
    public function deviceType($a): RivrRequestBuilder
    {
        if (!isset(self::VALID_DEVICE_TYPES[$a])) {
            LoggingMiddleware::debug("Unknown deviceType");
        }
        $this->DeviceType = $a;
        return $this;
    }

    public function libCallId(int $a): RivrRequestBuilder
    {
        $this->LibCallId = $a;
        return $this;
    }

    public function location(string $a): RivrRequestBuilder
    {
        $this->Location = $a;
        return $this;
    }

    public function locationType(?int $a): RivrRequestBuilder
    {
        $this->LocationType = $a;
        return $this;
    }

    public function operatingSystem($a): RivrRequestBuilder
    {
        if (stripos($a, 'chrome') !== false) {
            $a = "Chrome";
        } elseif (stripos($a, 'ios') !== false) {
            $a = "iOS";
        } elseif (stripos($a, 'android') !== false) {
            $a = "Android";
        } elseif (stripos($a, 'linux') !== false) {
            $a = "Linux";
        } elseif (stripos($a, 'mac') !== false) {
            $a = "Mac-OS";
        } elseif (stripos($a, 'win') !== false) {
            $a = "Windows";
        } else {
            $a = "Other";
        }
        $this->OperatingSystem = $a;
        return $this;
    }

    public function operatingSystemVersion(?string $a): RivrRequestBuilder
    {
        $this->OperatingSystemVersion = $a;
        return $this;
    }

    public function userId(string $a): RivrRequestBuilder
    {
        $this->UserId = $a;
        return $this;
    }

    public function additionalProperty(string $key, string $value): RivrRequestBuilder
    {
        $this->AdditionalProperties[strtolower($key)] = $value;
        return $this;
    }

    public function getAdSpotId(): ?string
    {
        return $this->AdSpotId;
    }

    public function getAppOrSite(): ?string
    {
        return $this->AppOrSite;
    }

    public function getAuctionType(): ?int
    {
        return $this->AuctionType;
    }

    public function getBrowser(): ?string
    {
        return $this->Browser;
    }

    public function getBrowserVersion(): ?string
    {
        return $this->BrowserVersion;
    }

    public function getCategories(): ?array
    {
        return $this->Categories;
    }

    public function getClientFloorPriceFlexible(): ?bool
    {
        return $this->ClientFloorPriceFlexible;
    }

    public function getClientFloorPrice(): ?float
    {
        return $this->ClientFloorPrice;
    }

    public function getContext()
    {
        return $this->Context;
    }

    public function getCountryCode(): ?string
    {
        return $this->CountryCode;
    }

    public function getCreativeTypes(): ?array
    {
        return $this->CreativeTypes;
    }

    public function getCreativeSizes(): ?array
    {
        return $this->CreativeSizes;
    }

    public function getConnectionType(): ?int
    {
        return $this->ConnectionType;
    }

    public function getDeviceLanguage(): ?string
    {
        return $this->DeviceLanguage;
    }

    public function getDeviceModel(): ?string
    {
        return $this->DeviceModel;
    }

    public function getDeviceManufacturer(): ?string
    {
        return $this->DeviceManufacturer;
    }

    public function getDeviceType()
    {
        return $this->DeviceType;
    }

    public function getLibCallId(): ?int
    {
        return $this->LibCallId;
    }

    public function getLocation(): ?string
    {
        return $this->Location;
    }

    public function getLocationType(): ?int
    {
        return $this->LocationType;
    }

    public function getOperatingSystem(): ?string
    {
        return $this->OperatingSystem;
    }

    public function getOperatingSystemVersion(): ?string
    {
        return $this->OperatingSystemVersion;
    }

    public function getUserId(): ?string
    {
        return $this->UserId;
    }

    public function getAdditionalProperties(): ?array
    {
        return $this->AdditionalProperties;
    }
}
