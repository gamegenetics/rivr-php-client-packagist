<?php

namespace Simplaex\Yieldoptimizer\Logging;

use Psr\Log\LoggerInterface;
use RuntimeException;

class LoggingMiddleware
{
    private static $logger = null;

    public static function initLogger(LoggerInterface $logger): void
    {
        if (self::$logger !== null) {
            throw new RuntimeException('Rivr logger has already been initialised!');
        }
        self::$logger = $logger;
    }

    // Logging
    public static function debug(string $message, array $context = []): void
    {
        if (self::$logger !== null) {
            self::$logger->debug($message, $context);
        }
    }

    public static function info(string $message, array $context = []): void
    {
        if (self::$logger !== null) {
            self::$logger->info($message, $context);
        }
    }

    public static function warning(string $message, array $context = []): void
    {
        if (self::$logger !== null) {
            self::$logger->warning($message, $context);
        }
    }

    public static function error(string $message, array $context = []): void
    {
        if (self::$logger !== null) {
            self::$logger->error($message, $context);
        }
    }
}
